from django.shortcuts import render, redirect,get_object_or_404
from todos.models import Todolist, Todoitem
from todos.forms import Listform, Itemform
from django.http import HttpResponseRedirect
# Create your views here.


def todo_list_list(request):
    todolist = Todolist.objects.all()
    context = {"todolists": todolist}
    return render(request, "todos/list.html", context)


def todo_list_detail(request, pk):
    context = {'todo': Todolist.objects.get(pk = pk) if Todolist else None,}
    return render(request, "todos/details.html", context)


def todo_list_create(request):
    context = {}
    form = Listform(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect("todo_list_list")
    context['form'] = form
    return render(request, "todos/create.html", context)


def update_list(request, pk):
    list = get_object_or_404(Todolist, pk=pk)
    form = Listform(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect("todo_list_list")
    context = {
        "form": form
    }
    return render(request, 'todos/update.html', context)

def delete_view(request, pk):
    context = {'todo' : Todolist.objects.all()}
    obj = get_object_or_404(Todolist, pk = pk)
    if request.method == 'POST':
        obj.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html", context)




def todo_item_create(request):
    context = {}
    form = Itemform(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect("todo_list_detail", Todoitem.list.id)
    context['form'] = form
    return render(request, "todos/todoitems/create.html", context)


def update_item(request, pk):
    list = get_object_or_404(Todoitem, pk=pk)
    form = Itemform(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect("todo_list_list")
    context = {
        "form": form
    }
    return render(request, 'todos/todoitems/update.html', context)
