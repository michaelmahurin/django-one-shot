from django.contrib import admin
from django.urls import include, path
from todos.views import todo_list_list, todo_list_detail, todo_list_create,update_list, delete_view,todo_item_create



urlpatterns = [
    path("", todo_list_list, name = "todo_list_list"),
    path('<int:pk>/', todo_list_detail, name = 'todo_list_detail'),
    path("create/", todo_list_create, name="todo_list_create"),
    path("<int:pk>/edit/",update_list,name="todo_list_update"),
    path("<int:pk>/delete/", delete_view, name="todo_list_delete"),
    path( "items/create/",todo_item_create,name="todo_item_create", ),


    




    
]
