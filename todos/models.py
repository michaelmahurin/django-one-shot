from tkinter import CASCADE
from os import truncate
from django.db import models

class Todolist(models.Model):
    name = models.CharField(max_length=100, null=True)
    created_on = models.DateTimeField(null=True)

    def __str__(self):
        return self.name
        

class Todoitem(models.Model):
    task = models.CharField(max_length=100)
    due_date = models.DateTimeField(null=True, blank=True)
    is_completed = models.BooleanField(default=False)
    list = models.ForeignKey(
        "Todolist", related_name= "items", on_delete=models.CASCADE, null=True)
    
    def __str__(self):
        return self.task