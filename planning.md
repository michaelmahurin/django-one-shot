Fork and clone the starter project from django-one-shot DONE
Create a new virtual environment in the repository directory for the project DONE
Activate the virtual environment DONE
Upgrade pip DONE
Install django DONE
Install black DONE     
Install flake8 DONE
Install djlint DONE
Deactivate your virtual environment DONE
Activate your virtual environment DONE
Use pip freeze to generate a requirements.txt file DONE
git add . DONE
git commit -m "Feature 2 Complete" DONE
git push origin main DONE
Create a Django project named brain_two so that the manage.py file is in the top directory DONE
Create a Django app named todos and install it in the brain_two Django project in the INSTALLED_APPS list DONE
Run the migrations DONE
Create a super user DONE