from django import forms
from .models import Todoitem, Todolist



class Listform(forms.ModelForm):

    class Meta:
        model = Todolist
        fields = ["name"]


class Itemform(forms.ModelForm):

    class Meta:
        model = Todoitem
        fields = ['task', "due_date", "is_completed", "list"]